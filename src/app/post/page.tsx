/* eslint-disable @next/next/no-img-element */

import React from 'react'
import Favorite from '@/components/Favorite'
import { Planet } from '@/icons/Planet'

const fetchData = async (option: string = "") => {

  const response = option === 'jsonplaceholder' ? await fetch('https://jsonplaceholder.typicode.com/posts') : await fetch("https://rickandmortyapi.com/api/character");
  const data = await response.json()
  return data
}
interface PostRick {
  id: number,
  name: string,
  status: string,
  species: string,
  image: string,
  origin: {
    name: string
  },
  location: {
    name: string
  }
}
interface Post {
  id: number,
  title: string,
  body: string
}
// {
//   id: 20,
//   name: 'Ants in my Eyes Johnson',
//   status: 'unknown',
//   species: 'Human',
//   type: 'Human with ants in his eyes',
//   gender: 'Male',
//   origin: { name: 'unknown', url: '' },
//   location: {
//     name: 'Interdimensional Cable',
//     url: 'https://rickandmortyapi.com/api/location/6'
//   },
//   image: 'https://rickandmortyapi.com/api/character/avatar/20.jpeg',
//   episode: [ 'https://rickandmortyapi.com/api/episode/8' ],
//   url: 'https://rickandmortyapi.com/api/character/20',
//   created: '2017-11-04T22:34:53.659Z'
// }
export default async function Posts() {
  let option = "jsonplaceholder"
  const result = await fetchData(option)
  // console.log(data.results);
  return (
    <div className='flex min-h-screen flex-col items-center justify-between p-24'>
      <h1>Post</h1>

      {option === "jsonplaceholder" ? (
        <div className="flex flex-col">
          {result.slice(0, 10).map((post: Post) => (
            <div key={post.id} className="flex flex-col border-2 border-gray-300 rounded-lg p-4 m-4">
              <h3 className="text-xl font-bold">{post.title}</h3>
              <p>{post.body}</p>
              <Favorite />
            </div>
          ))}
        </div>
      ) : (
        <div className="grid grid-cols-2 gap-5">
          {
            result.results.slice(0, 10).map((post: PostRick) => (
              <figure className="relative md:flex bg-slate-100 rounded-xl p-8 md:p-0 dark:bg-slate-800" key={post.id}>
                <img className="w-24 h-24 md:w-48 md:h-auto md:rounded-none rounded-full mx-auto" src={post.image} alt={post.name} width="384" height="512" />
                <p className="absolute text-2xl font-extrabold  ordinal bg-clip-text text-transparent bg-gradient-to-r from-pink-500 to-violet-500">{post.id}st</p>
                <div className="pt-6 md:p-8 text-center md:text-left space-y-4">
                  <blockquote>
                    <p className="text-lg font-medium">
                      “Tailwind CSS is the only framework that Ive seen scale
                      on large teams.”
                    </p>
                    <ul className='text-lg font-medium'>
                      <li >{post.origin.name == ('Earth (Replacement Dimension)') ? (post.origin.name) : post.origin.name} </li>
                      <li className={post.location.name === 'unknown' ? "text-amber-500" : "text-indigo-600"}>{post.location.name}</li>
                    </ul>
                  </blockquote>
                  <figcaption className="font-medium">
                    <div className="text-sky-500 dark:text-sky-400">
                      {post.name}
                      <span className={post.status === 'Alive' ? "ml-3 text-sky-700" : post.status === 'Dead' ? "ml-3 text-red-700 line-through" : "ml-3 text-amber-500"}>
                        {`(${post.status})`}
                      </span>
                    </div>

                    <div className={post.species === 'Human' ? "text-slate-500 dark:text-slate-400" : "text-alien"}>
                      {post.species}
                    </div>
                  </figcaption>
                </div>
                <Favorite />
              </figure>
            ))
          }
        </div>
      )}


    </div>
  )
}

