import React from 'react'

export default function DetailPostPage({ params }: { params: { id: string } }) {
    return (
        <div className='flex min-h-screen flex-col items-center justify-between p-24'>
            Detail Post Page {params.id}
        </div>
    )
}
