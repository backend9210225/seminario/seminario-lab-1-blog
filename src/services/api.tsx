import { RequestOptions, header } from './models/models'

const request = async (requestOptions: RequestOptions) => {
    const { endpoint, method, options, isFile } = requestOptions;
    options === undefined ? requestOptions.options = {} : requestOptions.options = options;
    isFile === undefined ? requestOptions.isFile = false : requestOptions.isFile = isFile;

    const headers = {
        'Content-Type': 'application/json',
    }
    let config: any = {
        method,
        headers,
        body: JSON.stringify(options)
    }

    if (isFile) {
        config.headers = {}
        config.body = options
    }

    try {
        const reponse = await fetch(endpoint, config)
        if (!reponse.ok) {
            throw Error(reponse.statusText)
        }

        const data = await reponse.json()
        return data
    } catch (error: any) {
        throw Error(error.message())
    }
}

export default request