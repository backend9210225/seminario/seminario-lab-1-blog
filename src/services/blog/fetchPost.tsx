import request from '../api'
import { Post } from '../models/models'

async function fetchPost() {
    const result = await request({
        endpoint: 'https://jsonplaceholder.typicode.com/posts',
        method: 'GET'
    })
    return result as Post[]
}

export default fetchPost