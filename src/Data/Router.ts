export type navItem = {
  path: string;
  content: string;
};
export const NavigationItems: navItem[] = [
  {
    path: "/",
    content: "Home",
  },
  {
    path: "/post",
    content: "Posts",
  },
  {
    path: "/about",
    content: "About",
  },
];
// export const NavigationItems: navItem[] = [
//   ...navbar,
//   {
//     path: "/sitemap",
//     content: "Sitemap",
//   },
// ];

export const ContactItems: navItem[] = [
  {
    path: "/contac",
    content: "Conectate con nosotros",
  },
  {
    path: "https://www.facebook.com/",
    content: "Facebook",
  },
  {
    path: "https://twitter.com/",
    content: "Twitter",
  },
  {
    path: "https://www.tiktok.com/",
    content: "TikTok",
  },
];
