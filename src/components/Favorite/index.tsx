"use client"
import { useState } from 'react'
import styles from './styles.module.css'

function Favorite() {
    const [favorite, setFavorite] = useState<boolean>(false)

    function handleClick() {
        setFavorite(!favorite)
    }
    return (
        <div className={styles.Favorite}>
            <button onClick={handleClick}>{favorite ? <div>💖</div> : <div>🤯</div>}</button>
        </div>
    )
}

export default Favorite