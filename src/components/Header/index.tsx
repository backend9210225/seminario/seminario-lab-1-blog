/* eslint-disable @next/next/no-img-element */

import styles from './styles.module.css'
export default function index() {
    return (
        <header className={styles.header}>
            <img className={styles.img} src="https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F6%2F2019%2F07%2Fram_723783287839232-2000.jpg" alt="Hero" />
        </header>
    )
}
