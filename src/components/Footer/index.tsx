/* eslint-disable @next/next/no-img-element */
import styles from './styles.module.css'
import { navItem } from '@/Data/Router'
import NavBar from '@/components/NavBar'
import { NextIcon } from '@/icons/NextIcon'
import { VercelIcon } from '@/icons/VercelIcon'
function Footer({ footerItems, contact }: { footerItems: navItem[], contact: navItem[] }) {
    return (
        <footer className='bg-gray-200 px-1 py-3'>
            <div className='container mx-auto px-4 flex justify-between items-start'>
                <NavBar navItem={footerItems} classname={'flex flex-col'} />
                <NavBar navItem={contact} classname={'flex flex-col'} />
                <NextIcon />
                <VercelIcon />
                <img width={100} height={100} src="seminario.jpeg" alt="" />
            </div>
        </footer>
    )
}

export default Footer
