import Link from 'next/link'
import styles from './styles.module.css'
import { navItem } from '@/Data/Router'
function NavBar({ navItem, classname }: { navItem: navItem[], classname?: string }) {
    classname = classname ? classname : styles.nav
    return (
        <nav className={classname}>
            <ul>
                {navItem.map((item) => (
                    <>
                        <li>
                            <Link href={item.path}>{item.content}</Link>
                        </li>
                    </>
                ))}
            </ul>
        </nav>
    )
}

export default NavBar